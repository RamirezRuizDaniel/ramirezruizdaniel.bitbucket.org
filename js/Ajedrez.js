var Ajedrez = (function(){
  // Variables locales
  var boton = document.createElement("input");
  var direccionCSV = 'https://ramirezruizdaniel.bitbucket.io/csv/tablero.csv';
  // Funciones locales  
  var _pintarTablero = function(pet){
    var divTab = document.getElementById("tablero");
    var respuesta = pet.response;
    var filas = respuesta.split("\n");
    var tabla = document.createElement("table");
    tabla.setAttribute("id","_tabla_juego")
    var cuerpo = document.createElement("tbody");
    cuerpo.setAttribute("id","_body_table");
    var letras = filas[0].split("|");
    var aux = 8;
    for(var j = 1;j < filas.length;j++){
      var celdas = filas[j].split("|");
      var fila = document.createElement("tr");
      for(var i = 0;i<celdas.length;i++){
        var celda = document.createElement("td");
        var cadena = ""+letras[i]+aux;
        celda.setAttribute("id", cadena);
        celda.innerHTML = celdas[i];
        fila.appendChild(celda);
      }
      cuerpo.appendChild(fila);
      aux--;
    }
    tabla.appendChild(cuerpo);
    divTab.appendChild(tabla);
  };

  var _mostrar_tablero = function(){
    var req = new XMLHttpRequest();
    req.onreadystatechange = function(){
      if (this.readyState === 4) {
        if (this.status === 200) {
          _pintarTablero(req);
        }
        if(this.status === 404){
          var errores = document.getElementById("mensaje");
          var mens_erro = document.createElement("h1"); 
          mens_erro.innerHTML = "ERROR SE PERDIO LA CONEXION CON EL SERVIDOR";
          errores.appendChild(mens_erro);  
        }
      }
    };
    req.open('GET', direccionCSV, true); 
    req.send();
    _agregar_ev_boton();
  };

  var _actualizar_tablero = function(){
    var peticion = new XMLHttpRequest();
    var aux = document.getElementById("tablero");
    aux.removeChild(aux.childNodes[1]);
    peticion.onreadystatechange = function(){
      if (this.readyState === 4) {
        if (this.status === 200) {
          _pintarTablero(peticion);
        } 
        if(this.status === 404){
          var errores = document.getElementById("mensaje");
          var mens_erro = document.createElement("h1"); 
          mens_erro.innerHTML = "ERROR SE PERDIO LA CONEXION CON EL SERVIDOR";
          errores.appendChild(mens_erro);  
        }
      }
    };
    peticion.open('GET', direccionCSV, true); 
    peticion.send();
  };

  var _agregar_ev_boton = function(){
    var _opcion = document.getElementById("tablero");
    var _opciones = document.getElementById("opcion");
    boton.setAttribute("type","button");
    boton.setAttribute("value","Actualizar");
    boton.addEventListener("click",_actualizar_tablero);
    _opcion.appendChild(boton);
  };

  var _moverPieza = function(movimiento){
    var errores = document.getElementById("mensaje");
    var origen = movimiento.de;
    var destino = movimiento.a;
    var celda_origen = document.getElementById(origen);
    var celda_destino = document.getElementById(destino);
    var pieza = celda_origen.textContent;

    if(celda_destino.innerHTML.localeCompare("\u2205")===0){
      if(celda_origen.innerHTML.localeCompare("\u2205")!==0){
        celda_origen.textContent = "\u2205";
        celda_destino.textContent = pieza;
      }else{
        var mens_erro = document.createElement("h1"); 
        mens_erro.innerHTML = "ERROR NO EXISTE NINGUNA PIEZA EN LA POSICION DE ORIGEN";
        errores.appendChild(mens_erro);  
      }   
    }else{
      var mens_erro = document.createElement("h1"); 
      mens_erro.innerHTML = "ERROR EL LUGAR AL QUE QUIERES MOVER LA PIEZA NO ESTA VACIO";
      errores.appendChild(mens_erro);
    }
    
  };

  return {
    "mostrarTablero": _mostrar_tablero,
    "actualizarTablero": _actualizar_tablero,
    "moverPieza": _moverPieza
  };
 })();